package com.currency.limit.controller;

public class LimitConfig {

    private int maximum;
    private int minimum;

    public LimitConfig(){}

    public LimitConfig(int maximum, int minimum) {
        this.setMaximum(maximum);
        this.setMinimum(minimum);
    }


    public int getMaximum() {
        return maximum;
    }

    public void setMaximum(int maximum) {
        this.maximum = maximum;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }
}
