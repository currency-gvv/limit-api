package com.currency.limit.controller;

import com.currency.limit.ConfigurationReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LimitConfigController {

    @Autowired
    private ConfigurationReader cr;

    @GetMapping(path = "/limit")
    public LimitConfig retrieveLimitConfiguration(){
        return new LimitConfig(cr.getMin(), cr.getMax());
    }
}
